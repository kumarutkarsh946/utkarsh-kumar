import lhapdf
2 import pineappl
3
4 pdf = lhapdf . mkPDF ("
NNPDF40_nnlo_as_01180 " ,0)
5 # load grid
6 grid = pineappl . grid . Grid . read
("
CMS_DY_14TEV_MLL_5000_COSTH
. pineappl . lz4 ")
7 # convolute
8 print ( grid . convolute_with_one
(2212 , pdf . xfxQ2 , pdf .
alphasQ2 ) )
