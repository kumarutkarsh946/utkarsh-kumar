import numpy as np
# Define constants for the test
pdf = lhapdf . mkPDF ("
NNPDF40_nnlo_as_01180 " ,0)
MIN_Q = 1
MAX_Q = 100000
MIN_X = 1e-8
MAX_X = 0.99999999
TOLERANCE = 1e-8
MAX_PDF_VALUE = 1000000
def test_pdf():
    # Test that the PDF values are within expected bounds
    for id in range(-6, 7):
        for Q in [MIN_Q, MAX_Q]:
            for x in np.linspace(MIN_X, MAX_X, num=100):
                pdf_value = pdf_library.pdf(id, x, Q)
                assert pdf_value >= 0, f"PDF value for x={x}, Q={Q}, id={id} is negative"
                assert pdf_value <= MAX_PDF_VALUE, f"PDF value for x={x}, Q={Q}, id={id} exceeds max value of {MAX_PDF_VALUE}"
                # Test that the PDF values are zero at x=1 within a certain tolerance
                if abs(x-1) < TOLERANCE:
                    assert abs(pdf_value) < TOLERANCE, f"PDF value for x=1 is non-zero: {pdf_value}"